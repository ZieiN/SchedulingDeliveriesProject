#ifndef INTERVAL_H
#define INTERVAL_H
#include <vector>
using namespace std;

class Interval{
public:
	int start_time; // Start time of the interval.
	int end_time; // End time of the interval.
	int num_of_orders; // The number of orders which needs to deliver at this interval.
	vector<int> order_execution_time; // The delivery time of the orders.
	Interval(int, int, int, vector<int>);
};
#endif
