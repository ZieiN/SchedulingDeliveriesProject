#ifndef SCHEDULER_H
#define	SCHEDULER_H
#include "Interval.h"
#include <set>
#include <vector>

#define NOTHING_TO_DO -2
#define CHARGING -1
#define NORMAL_ORDER 0

class Scheduler{ // Each Scheduling instance contains the folllowing:
public:
	int num_of_robots; // The number of robots which will be used to deliver orders.
	int start_time; // The start time of the working day.
	int end_time; // The end time of the working day.
	vector<vector<pair<int, int>>> robots_jobs; // Vector to store the robots task. For each robot, 
	// it contains vector of pairs (explain the tasks) which store the time of the task (in the first value of the pair) and  the type of the task (in the second value of the pair).
	vector<vector<int>> robots_idle_times; // Vector to store the idle time of the robot before each task.
	vector<int> order_execution_time; // Vector to store the delivery time of the orders.
	vector<Interval> intervals; // Vector of class 'Interval' to store the intervals.
	Scheduler(); // Empty constructor.
	Scheduler(int, int, int, int, int, int, int, vector<int>, vector<int>); // constructor to build the Scheduler.
	void distribute_orders(); // Function to assign the orders to the robots and determine the times to start each order.
	void add_interval(int, int, int, vector<int>); // Function to add the interval information to the Scheduler.
private:
	int num_of_orders;
	int recharging_time;
	int max_continuous_working_time;
	int num_of_intervals;
	multiset<pair<int, int>> robots;
	vector<bool> is_order_delivered;
	vector<int> continuous_time;
	vector<int> time_create_order;
	vector<bool> is_interval_done;
	int next_order(int);
	bool does_this_order_affect_interval(int, int);
	void fulfill_interval(int);
	void fulfill_order(int, pair<int, int>);
	void complete_the_robots_by_idle_time();
};
#endif
