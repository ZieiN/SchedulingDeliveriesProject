
/* This is random input generator which takes the random seed from input, and prints a random instance which 
agrees with the values of the environment parameters in 'config.ini' file and the limits defined globally below.
*/
#include<bits/stdc++.h>
using namespace std;

// Limits to be specified.
const int max_time_order = 50, min_time_order = 10, max_num_orders = 300, max_num_of_intervals = 4, max_num_orders_at_interval = 20, min_num_orders_at_interval = 10,
					min_length_of_interval = 60, max_length_of_interval = 120, min_time_bw_intervals = 60;

map<string, int> read_config_parameters(){
	ifstream file;
	file.open("config.ini");
	string line, word;
	map<string, int> mp;
	while(getline(file, line)){
		istringstream iss(line);
		vector<string> v;
		while(iss>>word){
			v.push_back(word);
		}
		if(*v.begin()=="#")
			continue;
		mp[*v.begin()] = stoi(*--v.end());
	}
	file.close();
	return mp;
}

int main(){
	int num_of_robots, start_time, end_time;
	auto mp = read_config_parameters();
	num_of_robots = mp["num_of_robots"];
	start_time = mp["start_time"];
	end_time = mp["end_time"];
	cerr<<"Constants inside the code:"<<endl;
	cerr<<" The environment parameters taken from config.ini:"<<endl;
	cerr<<"  Num of robots = "<<num_of_robots<<endl;
	cerr<<"  Start working time = "<<start_time<<endl;
	cerr<<"  End working time = "<<end_time<<endl;
	cerr<<" Limits inside the code:"<<endl;
	cerr<<"  maximum delivery time of one order = "<<max_time_order<<endl;
	cerr<<"  minimum delivery time of one order = "<<min_time_order<<endl;
	cerr<<"  maximum number of orders in one day = "<<max_num_orders<<endl;
	cerr<<"  maximum number of intervals in one day = "<<max_num_of_intervals<<endl;
	cerr<<"  maximum number of orders in one interval = "<<max_num_orders_at_interval<<endl;
	cerr<<"  minimum number of orders in one interval = "<<min_num_orders_at_interval<<endl;
	cerr<<"  minimum time between two consequtive intervals = "<<min_time_bw_intervals<<endl;
	cerr<<"  minimum length of one interval = "<<min_length_of_interval<<endl;
	cerr<<"  maximum length of one interval = "<<max_length_of_interval<<endl;
	cerr<<" -------------------------------------------- "<<endl;
	int ssrand;
	cerr<<"Enter random seed: ";
	cin>>ssrand;
	srand(ssrand);
	int num_of_orders = rand()%max_num_orders+1;
	printf("%d\n",num_of_orders);
	for(int i=0; i<num_of_orders; ++i){
		int order_time = rand()%(max_time_order-min_time_order)+min_time_order;
		int arrive_time = rand()%(end_time - order_time);
		printf("%d %d\n",order_time, arrive_time);
	}
	int num_of_intervals = rand()%(max_num_of_intervals)+1;
	int past_interval_end = start_time;
	vector<int> v1[max_num_of_intervals];
	pair<int, pair<int, int>> v[max_num_orders_at_interval];
	for(int j=0; j<num_of_intervals; ++j){
		int num_of_orders_at_this_interval = rand()%(max_num_orders_at_interval-min_num_orders_at_interval)+min_num_orders_at_interval;
		int start_interval = past_interval_end + min_time_bw_intervals+rand()%(max_length_of_interval-past_interval_end-min_time_bw_intervals);
		int end_interval = min(end_time, start_interval + min_length_of_interval+rand()%(max_length_of_interval-min_length_of_interval));
		if(end_interval <= start_interval){
			num_of_intervals = j;
			break;
		}
		v[j] = {num_of_orders_at_this_interval, {start_interval, end_interval}};
		for(int i=0; i<num_of_orders_at_this_interval; ++i){
			int order_time = rand()%(max_time_order-min_time_order)+min_time_order;
			v1[j].push_back(order_time);
		}
		past_interval_end = end_interval;
	}
	cout<<num_of_intervals<<endl;
	for(int i=0; i<num_of_intervals; ++i){
		cout<<v[i].first<<" "<<v[i].second.first<<" "<<v[i].second.second<<endl;
		for(auto it:v1[i]){
			cout<<it<<endl;
		}
	}
}
