#include "Interval.h"
#include <vector>
using namespace std;

// Constructor to build the intervals.
Interval::Interval(int n_orders, int st_time, int nd_time, vector<int> order_exec_time){
	num_of_orders = n_orders;
	start_time = st_time;
	end_time = nd_time;
	order_execution_time = order_exec_time;	
}