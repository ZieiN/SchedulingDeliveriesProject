#include"Scheduler.h"
#include<bits/stdc++.h>
using namespace std;

Scheduler::Scheduler(){
	num_of_robots = 0;
	num_of_orders = 0;
	start_time = 0;
	end_time = 0;
	recharging_time = 0;
	max_continuous_working_time = 0;
	num_of_intervals = 0;
	intervals.clear();
	order_execution_time.clear();
	time_create_order.clear();
	continuous_time.clear();
	robots_jobs.clear();
	robots_idle_times.clear();
	is_order_delivered.clear();
	robots.clear();
}

Scheduler::Scheduler(int n_robots, int n_orders, int st_time, int nd_time, int rchrg_time, int max_continuous_time, int n_intervals, vector<int> order_exec_time, vector<int> create_time){
	num_of_robots = n_robots;
	num_of_orders = n_orders;
	start_time = st_time;
	end_time = nd_time;
	recharging_time = rchrg_time;
	max_continuous_working_time = max_continuous_time;
	num_of_intervals = n_intervals;
	intervals.clear();
	order_execution_time = order_exec_time;
	time_create_order = create_time;
	continuous_time = vector<int>(num_of_robots+1, 0);
	robots_jobs = vector<vector<pair<int, int>>>(num_of_robots+1, vector<pair<int,int>>());
	robots_idle_times = vector<vector<int>>(num_of_robots+1, vector<int>());
	is_order_delivered = vector<bool>(num_of_orders+1, false);
	for(int i=0; i<num_of_robots; ++i){
		robots.insert({start_time, i+1});
	}
}
// Function to add the interval information to the Scheduler instance.
void Scheduler::add_interval(int n_orders, int st_time, int nd_time, vector<int> order_exec_time){
	intervals.push_back(Interval(n_orders, st_time, nd_time, order_exec_time));
	is_interval_done.push_back(false);
}

// This function returns the next order to distribute (according the algorithm explained in description)
// The next order is chosen as follows:
// 1- Sort the orders by the create_time ascendingly.
// 2- Break the tie by sorting them by the time to deliver the order ascendingly.
// 3- Choose the first available order from them after sorting.
int Scheduler::next_order(int robot_time){
	vector<pair<pair<int, int>, int>> orders;
	for (int order_i=0; order_i<num_of_orders; ++order_i){
		if(is_order_delivered[order_i]){
			continue;
		}
		orders.push_back({{time_create_order[order_i], order_execution_time[order_i]}, order_i});
	}
	sort(orders.begin(), orders.end());
	return orders.begin()->second;
}
// Function to check if the next order affects the orders at intervals.
// Affecting the orders, mean that at least one undelivered order at one interval has delayed if the next order has fulfilled.
bool Scheduler::does_this_order_affect_interval(int order_i, int interval_i){
	auto chosen_robot = robots.begin();
	int robot_new_time = max(chosen_robot->first, time_create_order[order_i]) + order_execution_time[order_i];
	if(robot_new_time + intervals[interval_i].order_execution_time[0] > intervals[interval_i].start_time){
		return true;
	}
	return false;
}
// Function to distribute the orders which must be delivered at the interval #'interval_i' to robots.
void Scheduler::fulfill_interval(int interval_i){
	vector<pair<int, int>> orders;
	for(int i=0; i<intervals[interval_i].num_of_orders; ++i){
		orders.push_back({intervals[interval_i].order_execution_time[i], i+1});
	}
	// Bring the longest 'num_of_robots' orders to the beginning, and then sort the remaining ascendingly.
	sort(orders.begin(), orders.end(), greater<pair<int,int>>());
	if(orders.size() > num_of_robots){
		reverse(orders.begin()+num_of_robots, orders.end());
	}
	// Every time choose the earliest robot, and assign the orders sequentially as ordered previously.
	for(int i=0; i<intervals[interval_i].num_of_orders; ++i){
		auto chosen_robot = *robots.begin();
		robots.erase(robots.find(chosen_robot));
		if(orders[i].first+continuous_time[chosen_robot.second] > max_continuous_working_time){
			robots_jobs[chosen_robot.second].push_back({recharging_time, CHARGING});
			robots_idle_times[chosen_robot.second].push_back(0);
			continuous_time[chosen_robot.second] = 0;
			chosen_robot.first += recharging_time;
		}
		auto robot_new_time = max(chosen_robot.first+ orders[i].first, intervals[interval_i].start_time);
		robots.insert({robot_new_time, chosen_robot.second});
		continuous_time[chosen_robot.second]+=orders[i].first;
		robots_jobs[chosen_robot.second].push_back({orders[i].second, interval_i+1});
		robots_idle_times[chosen_robot.second].push_back(robot_new_time - chosen_robot.first - orders[i].first);
	}
	is_interval_done[interval_i] = true;
}
// Assign an order to a robot.
// order_i is the id of the order.
// chosen_robot is a std::pair<int,int> which consists of \
 the time when the current robot finishs all their assigned tasks (in the first value of the pair),\
 and the id of the robot (in the second value of the pair).
void Scheduler::fulfill_order(int order_i, pair<int,int> chosen_robot){
	robots.erase(robots.find(chosen_robot));
	// We check if the robot can make the order before it becomes out of charge or not.
	// If if can't do the order, we first charge it.
	if(order_execution_time[order_i]+continuous_time[chosen_robot.second] > max_continuous_working_time){
			robots_jobs[chosen_robot.second].push_back({recharging_time, CHARGING});
			robots_idle_times[chosen_robot.second].push_back(0);
			chosen_robot.first+=recharging_time;
			continuous_time[chosen_robot.second] = false;
		}
		int robot_new_time = max(chosen_robot.first, time_create_order[order_i]) + order_execution_time[order_i];
		continuous_time[chosen_robot.second] += order_execution_time[order_i];
		robots_jobs[chosen_robot.second].push_back({order_i, NORMAL_ORDER});
		robots_idle_times[chosen_robot.second].push_back(robot_new_time - order_execution_time[order_i] - chosen_robot.first);
		robots.insert({robot_new_time, chosen_robot.second});
		is_order_delivered[order_i]=true;
}
// If the robots finished its tasks before the end of the working time of the day, assign idle times to it to the end of the working time.
void Scheduler::complete_the_robots_by_idle_time(){
	for(auto it:robots){
		if(it.first < end_time){
			robots_idle_times[it.second].push_back(end_time-it.first);
			robots_jobs[it.second].push_back({0, NOTHING_TO_DO});
		}
	}
}
// This is the main function which assign the orders to the robots.
void Scheduler::distribute_orders(){
	for(int i=0; i<num_of_orders; ++i){
		// Choose the robot to give the it the next order. Here we choose the earliest free one.
		auto chosen_robot = *robots.begin();
		// Choose the next order according to next_order function.
		auto order_i = next_order(chosen_robot.first);
		// Flag to detect if the current order affect at least one order at any interval.
		bool some_interval_fulfilled = false;
		for(int interval_i=0; interval_i<num_of_intervals; ++interval_i){
			if(is_interval_done[interval_i])
				continue;
			if(does_this_order_affect_interval(order_i, interval_i)){
				fulfill_interval(interval_i);
				some_interval_fulfilled = true;
				break;
			}	
		}
		// If some interval has fulfilled, we reconsider choosing the suitable robot because their availabilities have changed.
		if(some_interval_fulfilled){
			--i;
			continue;
		}
		// Else we fullfill the next order by the chosen robot by the function fulfill_order.
		fulfill_order(order_i, chosen_robot);
	}
	// If there are some intervals still have orders to deliver, deliver them.
	for(int interval_i=0; interval_i<num_of_intervals; ++interval_i){
		if(!is_interval_done[interval_i]){
			fulfill_interval(interval_i);
		}
	}
	// We complete the robots which finished all their task before the end of working day, by idle time.
	complete_the_robots_by_idle_time();
}
