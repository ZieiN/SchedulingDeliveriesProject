#include<bits/stdc++.h>
#include "Scheduler.h"
#include "Interval.h"
using namespace std;

map<string, int> read_config_parameters(){
	freopen("../../config.ini","r",stdin);
	string line, word;
	map<string, int> mp;
	while(getline(cin, line)){
		istringstream iss(line);
		vector<string> v;
		while(iss>>word){
			v.push_back(word);
		}
		if(*v.begin()=="#")
			continue;
		mp[*v.begin()] = stoi(*--v.end());
	}
	fclose(stdin);
	return mp;
}

// Read all the input data and return a Scheduler instance containing all of them.
Scheduler read_input(istream& in){
	int n_robots, n_orders, st_time, nd_time, n_intervals, rchrg_time, mx_continuous_time;
	auto mp = read_config_parameters();
	n_robots = mp["num_of_robots"];
	st_time = mp["start_time"];
	nd_time = mp["end_time"];
	mx_continuous_time = mp["max_continuous_working_time"];
	rchrg_time = mp["recharging_time"];
	vector<int> vector_order_exec_time, vector_order_create_time;
	in>>n_orders; // Third line, read the number of orders in all day.
	int order_exec_time, order_create_time;
	for(int i=0; i<n_orders; ++i){ // For every order, read the following:
		in>>order_exec_time>>order_create_time; // Read the time needed to deliver the order and when the order arrives to the system.
		vector_order_exec_time.push_back(order_exec_time);
		vector_order_create_time.push_back(order_create_time);
	}
	in>>n_intervals; // Read the number of intervals of times when some orders must be delivered.
	Scheduler Sch(n_robots, n_orders, st_time, nd_time, rchrg_time, mx_continuous_time, n_intervals, vector_order_exec_time, vector_order_create_time);
	for(int i=0; i<n_intervals; ++i){ // For each interval, read the following:
		in>>n_orders>>st_time>>nd_time; // Read the number of orders to deliver in this intervl, the start time of the interval and the final time of the interval.
		vector_order_exec_time.clear();
		for(int j=0; j<n_orders; ++j){ // For each order in the interval, read the following:
			in>>order_exec_time; // Read the time needed to delive the order.
			vector_order_exec_time.push_back(order_exec_time);
		}	
		Sch.add_interval(n_orders, st_time, nd_time, vector_order_exec_time);
	}
	return Sch;
}


// Function to print the explanation of the scheduling.
void print_output(Scheduler Sch){
	for(int i=1; i<=Sch.num_of_robots; ++i){
		int sum = 0;
		printf("Robot #%d:\n", i);
		for(int j=0; j<Sch.robots_jobs[i].size(); ++j){
			if(Sch.robots_idle_times[i][j]>0){
				printf("Idle time for: %d minutes\n", Sch.robots_idle_times[i][j]);
				sum+=Sch.robots_idle_times[i][j];
			}
			if(Sch.robots_jobs[i][j].second == NOTHING_TO_DO)
				continue;
			if(Sch.robots_jobs[i][j].second == CHARGING){
				printf("Change the battery for %d minutes.\n", Sch.robots_jobs[i][j].first);
			}
			else if(Sch.robots_jobs[i][j].second == NORMAL_ORDER){
				printf("Do the order #%d for %d minutes", Sch.robots_jobs[i][j].first, Sch.order_execution_time[Sch.robots_jobs[i][j].first]);
				sum += Sch.order_execution_time[Sch.robots_jobs[i][j].first];
				if(sum>Sch.end_time){
					printf(" -- fail to deliver in working time");
				}
				printf(".\n");
			}
			else{
				int interval_i = Sch.robots_jobs[i][j].second-1;
				printf("Do the order #%d in interval %d for %d minutes", Sch.robots_jobs[i][j].first, interval_i+1, Sch.intervals[interval_i].order_execution_time[Sch.robots_jobs[i][j].first-1]);
				sum += Sch.intervals[interval_i].order_execution_time[Sch.robots_jobs[i][j].first];
				if(sum>Sch.intervals[interval_i].end_time){
					printf(" -- fail to deliver in interval %d", interval_i);
				}
				printf(".\n");
			}
		}
		printf("\n");
	}
}
int main(int argc, char *argv[]){
	Scheduler Sch;
	if (argc > 1) {     // read from file if given as argument 
        ifstream fin (argv[1]);
        if (fin.is_open()){
			Sch = read_input(fin);
		}
        else {
            std::cerr << "error: file open failed.\n";
            return 1;
        }
    }
    else {  // read from stdin 
        Sch = read_input(cin);
    }
	Sch.distribute_orders(); // Distribute the orders on the robots, and determine when each robot must do their orders.
	print_output(Sch);
}
